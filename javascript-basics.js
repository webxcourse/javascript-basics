let num_1 = 7;
let str = "7";
let bool = true; // false

// תנאי
if (bool) {
    console.log('אכן כן');
}
else {
    console.log('וואלה לא');
}

// ביטוי בוליאני
let num_2 = 8;
num_2 = num_1;

if (num_1 === num_2) {
    console.log('אכן כן');
}
else {
    console.log('וואלה לא');
}

// הגדרת פונקציה
function log_add_3(num) {
    console.log(num + 3);
}

// שימוש בפןנקציה
log_add_3(7);

// לןלאה
for (let i = 0; i < 5 ; i = i + 1) {
    // הקוד הזה ירוץ 5 פעמים
    console.log('i:', i);
}

/*

שיעורי בית:
1. להגדיר 5 משתנים עם המילים "מה" "המצב" וכו וליצור משפט כל שהוא - ואז להדפיס אותו
2. להגדיר משתנים, ולהשתמש בהם בתוך ביטוי בוליאני, בתוך תנאי if כדי להדפיס או לא להדפיס משהו. נא ליצור 3 if-ים כאלה. לדוגמה:
let a = 1;
if (a === 1) {
    console.log("אני מדפיס משהו")
}


2. לעבור על הדף הזה ולפתור את התרגיל שבסופו
https://www.w3schools.com/js/js_variables.asp

*/
